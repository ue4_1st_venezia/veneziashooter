// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickUp.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "ShooterCharacter.h"
#include "Gun.h"

#pragma region Contructor
// Sets default values
AAmmoPickUp::AAmmoPickUp()
{
	// Set this actor to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	Sphere->SetSphereRadius(50.f);
	Sphere->SetGenerateOverlapEvents(true);
	Sphere->SetupAttachment(Root);
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &AAmmoPickUp::OnOverlapBegin);

}
#pragma endregion

#pragma region OnOverlapBegin
void AAmmoPickUp::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndexType, bool bFromSweep, const FHitResult& SweepResult)
{
	AShooterCharacter* Char = Cast<AShooterCharacter>(OtherActor);

	if (Char != nullptr)
	{
		Char->Gun->AddAmmo();
	}

	Destroy();
}
#pragma endregion