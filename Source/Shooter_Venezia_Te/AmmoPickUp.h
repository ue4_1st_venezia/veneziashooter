// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AmmoPickUp.generated.h"

class AGun;

UCLASS()
class SHOOTER_VENEZIA_TE_API AAmmoPickUp : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAmmoPickUp();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite) //possiamo editare in editor solo i valori di default ma non in runtime
		TSubclassOf<AGun> GunClassAmmo;

	UPROPERTY(EditAnywhere)
		USceneComponent* Root;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		class USphereComponent* Sphere;

	UPROPERTY()
		AGun* GunAmmo;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndexType, bool bFromSweep, const FHitResult& SweepResult);
};