// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

class AGrenadeLauncherProjectile;

UCLASS()
class SHOOTER_VENEZIA_TE_API AGun : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGun();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isRiffle;

#pragma region Riffle Ammo
	void PullRiffleTrigger();

	UPROPERTY(EditAnywhere, Category = "Riffle") //proiettili massimi
		int MaxRiffleAmmo = 200;

	UPROPERTY()
		int RiffleAmmoAmount; //ammontare dei proiettili

	UPROPERTY(EditAnywhere, Category = "Riffle") // proiettili da sparare
		int RiffleAmmo = 10;

	UPROPERTY(EditAnywhere, Category = "Riffle") // proiettili sparati
		int RiffleAmmoShoot = 0;

	UPROPERTY(EditAnywhere, Category = "Riffle") // proiettili sparati
		int RiffleAmmoRefill = 20;
#pragma endregion

#pragma region GrenadeLauncher Ammo
	void PullGrenadeLauncherTrigger();

	UPROPERTY(EditAnywhere, Category = "GrenadeLauncher") //proiettili massimi
		int MaxGrenadeLauncherAmmo = 4;

	UPROPERTY()
		int GrenadeLauncherAmmoAmount; //ammontare dei proiettili

	UPROPERTY(EditAnywhere, Category = "GrenadeLauncher") // proiettili da sparare
		int GrenadeLauncherAmmo = 2;

	UPROPERTY(EditAnywhere, Category = "GrenadeLauncher") // proiettili sparati
		int GrenadeLauncherAmmoShoot = 0;

	UPROPERTY(EditAnywhere, Category = "GrenadeLauncher") // proiettili sparati
		int GrenadeLauncherAmmoRefill = 2;
#pragma endregion

	UFUNCTION()
		void AddAmmo();

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* Mesh;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "State")
		class AShooterCharacter* CurrentOwner;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);

	AController* GetOwnerController() const;
private:

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	UPROPERTY(EditAnywhere)
		UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere)
		USoundBase* MuzzleSound;

	UPROPERTY(EditAnywhere)
		UParticleSystem* ImpactEffect;

	UPROPERTY(EditAnywhere)
		USoundBase* ImpactSound;

	UPROPERTY(EditAnywhere)
		float MaxRange = 1000;

	UPROPERTY(EditAnywhere)
		float Damage = 10;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AGrenadeLauncherProjectile> GrenadeProjectile;

public:
	UPROPERTY()
		AGrenadeLauncherProjectile* Grenade;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		FVector MuzzleOffset;
};