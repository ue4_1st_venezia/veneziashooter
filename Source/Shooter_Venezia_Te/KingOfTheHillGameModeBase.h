// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Shooter_Venezia_TeGameModeBase.h"
#include "KingOfTheHillGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_VENEZIA_TE_API AKingOfTheHillGameModeBase : public AShooter_Venezia_TeGameModeBase
{
	GENERATED_BODY()

private:
	virtual void BeginPlay() override;

public:

	void CharacterHasMaxPoints();
	virtual void PawnKilled(APawn* PawnKilled) override;

	void EndGame(bool bIsPlayerWinner);

private:

	//Spawn a Spawner
	UPROPERTY(EditAnywhere)
		FVector SpawnerVector;

	UPROPERTY(EditAnywhere)
		FRotator SpawnerRotator;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AEnemySpawnerActor> SpawnerActor;

	//Spawn a End Game Hill
	UPROPERTY(EditAnywhere)
		FVector HillVector;

	UPROPERTY(EditAnywhere)
		FRotator HillRotator;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AHillActor> HillActor;
};