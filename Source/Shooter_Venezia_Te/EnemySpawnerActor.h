// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawnerActor.generated.h"

UCLASS()
class SHOOTER_VENEZIA_TE_API AEnemySpawnerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawnerActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AShooterCharacter> EnemyDeathmatch;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AShooterCharacter> EnemyKingOfTheHill;

	class AShooterCharacter* KingOfTheHillEnemy;
	
private:
	void SpawnDeathmatchAI();
	void SpawnKingOfTheHillAI();
};
