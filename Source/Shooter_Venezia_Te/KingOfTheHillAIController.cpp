// Fill out your copyright notice in the Description page of Project Settings.


#include "KingOfTheHillAIController.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HillActor.h"
#include "ShooterCharacter.h"

void AKingOfTheHillAIController::BeginPlay()
{
	Super::BeginPlay();

	if (AIBehaviour != nullptr)
		RunBehaviorTree(AIBehaviour);

	AActor* Gino = UGameplayStatics::GetActorOfClass(GetWorld(), HillActor);
	
		GetBlackboardComponent()->SetValueAsObject(TEXT("EndLevel"), Gino);
}

bool AKingOfTheHillAIController::IsDead() const
{
	AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn());

	if (ControlledCharacter != nullptr)
		return ControlledCharacter->IsDead();


	return true;
}


void AKingOfTheHillAIController::Dead()
{
	AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn());
	float Death = 0;
	if (ControlledCharacter != nullptr)
	{
		ControlledCharacter->Health = Death;
		ControlledCharacter->IsDead();
	}
}