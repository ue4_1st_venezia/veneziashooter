// Fill out your copyright notice in the Description page of Project Settings.

#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h" //classe che ha i gizmos per debuggare
#include "GrenadeLauncherProjectile.h"


#pragma region Constructor
// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}
#pragma endregion

#pragma region BeginPlay
// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
	RiffleAmmoAmount = MaxRiffleAmmo;
	GrenadeLauncherAmmoAmount = MaxGrenadeLauncherAmmo;

	if (!CurrentOwner)
		Mesh->SetVisibility(false);

	isRiffle = true;
}
#pragma endregion

#pragma region GunTrace
bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr) return false;

	FVector Location;
	FRotator Rotation;

	OwnerController->GetPlayerViewPoint(Location, Rotation);
	ShotDirection = -Rotation.Vector();
	FVector End = Location + Rotation.Vector() * MaxRange;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	return GetWorld()->LineTraceSingleByChannel(Hit, Location, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}
#pragma endregion

#pragma region GetOwnerController

AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr) return nullptr;

	return OwnerPawn->GetController();
}

#pragma endregion

#pragma region AddAmmo
void AGun::AddAmmo()
{
	if (isRiffle == true)
		RiffleAmmoAmount += RiffleAmmoRefill;
	else
		GrenadeLauncherAmmoAmount += GrenadeLauncherAmmoRefill;
}
#pragma endregion

#pragma region PullTrigger
void AGun::PullRiffleTrigger()
{
	if (RiffleAmmo > 0)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
		UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

		FHitResult Hit;
		FVector ShotDirection;

		bool bSuccess = GunTrace(Hit, ShotDirection);
		if (!bSuccess) return;

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ImpactSound, Hit.Location, ShotDirection.Rotation());

		AActor* HitActor = Hit.GetActor();
		
		if (HitActor != nullptr)
		{
			FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);

			AController* OwnerController = GetOwnerController();
			HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
		}

		RiffleAmmo--;
		RiffleAmmoShoot++;

	}
}

void AGun::PullGrenadeLauncherTrigger()
{
	if (GrenadeLauncherAmmo > 0)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
		UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));
		
		FHitResult Hit;
		FVector ShotDirection;
	
		bool bSuccess = GunTrace(Hit, ShotDirection);
		if (!bSuccess) return;

		UWorld* const World = GetWorld();
		World->SpawnActor<AGrenadeLauncherProjectile>(GrenadeProjectile, Hit.Location, ShotDirection.Rotation());

		GrenadeLauncherAmmo--;
		GrenadeLauncherAmmoShoot++;
	}
}
#pragma endregion