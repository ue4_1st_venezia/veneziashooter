// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "KingOfTheHillAIController.generated.h"

/**
 *
 */
UCLASS()
class SHOOTER_VENEZIA_TE_API AKingOfTheHillAIController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	void Dead();
	bool IsDead() const;

private:
	UPROPERTY(EditAnywhere)
		class UBehaviorTree* AIBehaviour;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AHillActor> HillActor;
};
