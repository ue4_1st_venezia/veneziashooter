// Fill out your copyright notice in the Description page of Project Settings.


#include "GrenadeLauncherProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "ShooterCharacter.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Gun.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AGrenadeLauncherProjectile::AGrenadeLauncherProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	SphereCollision->InitSphereRadius(5.0f);
	SphereCollision->BodyInstance.SetCollisionProfileName("Projectile");
	SphereCollision->SetGenerateOverlapEvents(true);
	SphereCollision->OnComponentHit.AddDynamic(this, &AGrenadeLauncherProjectile::OnHit);	
	RootComponent = SphereCollision;

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = SphereCollision;
	ProjectileMovement->InitialSpeed = 5000.f;
	ProjectileMovement->MaxSpeed = 5000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	
	InitialLifeSpan = .5f;

}

AController* AGrenadeLauncherProjectile::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr) return nullptr;

	return OwnerPawn->GetController();
}

void AGrenadeLauncherProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
	}

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, NormalImpulse.Rotation());
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ImpactSound, Hit.Location, NormalImpulse.Rotation());

	if (OtherActor != nullptr)
	{
		FPointDamageEvent DamageEvent(Damage, Hit, NormalImpulse, nullptr);		
	
		AController* OwnerController = GetOwnerController();
		OtherActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
	}
}


