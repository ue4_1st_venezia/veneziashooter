// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

class AGun;

UCLASS()
class SHOOTER_VENEZIA_TE_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()


public:
	// Sets default values for this character's properties
	AShooterCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintPure) //BlueprintPure non ha execution pin (la freccia bianca)  e va a braccietto con constant 
		bool IsDead() const;
	UFUNCTION(BlueprintPure)
		float GetHealthPercent() const;

	//Riffle Ammo
	UFUNCTION(BlueprintPure)
		int GetRiffleAmmo() const;

	UFUNCTION(BlueprintPure)
		int GetRiffleAmmoAmount() const;

	// GrenadeLaunchger Ammo
	UFUNCTION(BlueprintPure)
		int GetGrenadeLauncherAmmo() const;

	UFUNCTION(BlueprintPure)
		int GetGrenadeLauncherAmmoAmount() const;

	UFUNCTION(BlueprintPure)
		float PointsABC() const;

	void Shoot();

	void Refill();

	void HealthRegen();

	UPROPERTY(EditDefaultsOnly) //possiamo editare in editor solo i valori di default ma non in runtime
		TArray<TSubclassOf<AGun>> GunClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AGun* Gun;

	UPROPERTY(EditAnywhere)
		bool bIsPlayer; // = true isPlayer-> false isEnemy

	UPROPERTY(EditAnywhere)
		float MaxPoint = 50;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Points = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		virtual void OnRep_CurrentWeapon(const class AGun* OldWeapon);

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "State")
		TArray<AGun*> Weapons;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "State")
		int32 CurrentIndex = 0;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "State")
		AGun* CurrentWeapon;

	UFUNCTION(BlueprintCallable)
		virtual void EquipWeapon(const int32 Index);

private:

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);
	void NextWeapon();
	void LastWeapon();
	void Pause();

	UPROPERTY(EditAnywhere)
		float RotationRate = 10;

	UPROPERTY(EditDefaultsOnly)
		float MaxHealth = 100;

	UPROPERTY(EditDefaultsOnly)
		float EnemyMaxHealth = 50;


	UPROPERTY(EditAnywhere)
		float HealthAmount = 20;

	bool bInPause = false;

public:
	UPROPERTY(VisibleAnywhere)
		float Health;

	float GetPoint();
	void AddPoint(float Value);
};