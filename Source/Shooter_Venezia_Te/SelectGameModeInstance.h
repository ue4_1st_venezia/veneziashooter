// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SelectGameModeInstance.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_VENEZIA_TE_API USelectGameModeInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void PlayGame(FString Option);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName MapName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString MapOption;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsDM = false;	
};
