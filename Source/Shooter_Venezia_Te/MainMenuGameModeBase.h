// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Shooter_Venezia_TeGameModeBase.h"

#include "MainMenuGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class SHOOTER_VENEZIA_TE_API AMainMenuGameModeBase : public AShooter_Venezia_TeGameModeBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

//
//private:
//	UPROPERTY(EditAnywhere)
//		TSubclassOf<class UUserWidget> MenuScreenClass;
//
//	UPROPERTY()
//		UUserWidget* HUDScreenWidget;
};
