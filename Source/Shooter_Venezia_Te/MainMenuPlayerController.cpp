// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuPlayerController.h"
#include "Blueprint/UserWidget.h"

void AMainMenuPlayerController::BeginPlay()
{
	Super::BeginPlay();

	MenuWidget = CreateWidget(this, MenuClass);

	if (MenuWidget != nullptr)
		MenuWidget->AddToViewport();
}