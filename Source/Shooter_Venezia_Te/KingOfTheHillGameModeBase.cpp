// Fill out your copyright notice in the Description page of Project Settings.

#include "KingOfTheHillGameModeBase.h"
#include "EnemySpawnerActor.h"
#include "GameFramework/Controller.h"
#include "EngineUtils.h"
#include "HillActor.h"
#include "ShooterCharacter.h"
#include "KingOfTheHillAIController.h"
#include "ShooterPlayerController.h"

void AKingOfTheHillGameModeBase::BeginPlay()
{
	GetWorld()->SpawnActor<AHillActor>(HillActor, HillVector, HillRotator);
	GetWorld()->SpawnActor<AEnemySpawnerActor>(SpawnerActor, SpawnerVector, SpawnerRotator);
}

void AKingOfTheHillGameModeBase::CharacterHasMaxPoints()
{
	//Non Funziona
	//float point = 0;
	//bool  bWin = false;

	APlayerController* PlayerController = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());

		AShooterCharacter* Gino = Cast<AShooterCharacter>(PlayerController->GetCharacter());
		//point = Gino->Points;

		if (Gino->Points >= Gino->MaxPoint)
		{
			AKingOfTheHillGameModeBase::EndGame(true);
		}
	//if(PlayerController != nullptr)
	//{
	//}

	//for (AKingOfTheHillAIController* KOTHAI : TActorRange<AKingOfTheHillAIController>(GetWorld()))
	//{
	//	AShooterCharacter* AI = Cast<AShooterCharacter>(KOTHAI->GetCharacter());
	//
	//	if (point >= AI->GetPoint())
	//	{
	//		bWin = true;
	//	}
	//}
	//
	//
	//AKingOfTheHillGameModeBase::EndGame(bWin);
}

void AKingOfTheHillGameModeBase::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());

	if (PlayerController != nullptr)
	{
		AKingOfTheHillGameModeBase::EndGame(false);
	}

	for (AKingOfTheHillAIController* KOTHAI : TActorRange<AKingOfTheHillAIController>(GetWorld()))
	{
		if (KOTHAI->IsDead() == false)
			return;	
	}


	AKingOfTheHillGameModeBase::EndGame(true);
}

void AKingOfTheHillGameModeBase::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		//Metodo con if
		//bool bIsPlayerController = Controller->IsPlayerController();

		//if (bIsPlayerWinner)
		//{
		//	Controller->GameHasEnded(nullptr, bIsPlayerController);
		//}
		//else
		//{
		//	Controller->GameHasEnded(nullptr, !bIsPlayerController);
		//}

		//Metodo con una riga
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}
