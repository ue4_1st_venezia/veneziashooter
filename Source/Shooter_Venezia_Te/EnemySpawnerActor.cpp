// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawnerActor.h"
#include "ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "KillEmAllGameMode.h"
#include "KingOfTheHillAIController.h"

// Sets default values
AEnemySpawnerActor::AEnemySpawnerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnemySpawnerActor::BeginPlay()
{
	Super::BeginPlay();

	AKillEmAllGameMode* SelectGameMode = Cast<AKillEmAllGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (SelectGameMode)
	{
		SpawnDeathmatchAI();
	}
	else
	{
		SpawnKingOfTheHillAI();
	}	
}

void AEnemySpawnerActor::SpawnDeathmatchAI()
{
	const FVector Location = GetActorLocation();
	const FRotator Rotation = GetActorRotation();

	GetWorld()->SpawnActor<AShooterCharacter>(EnemyDeathmatch, Location, Rotation);
}

void AEnemySpawnerActor::SpawnKingOfTheHillAI()
{
	const FVector Location = GetActorLocation();
	const FRotator Rotation = GetActorRotation();

	KingOfTheHillEnemy = GetWorld()->SpawnActor<AShooterCharacter>(EnemyKingOfTheHill, Location, Rotation);	
}
