// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "ShooterCharacter.h"

void AShooterAIController::BeginPlay()
{
	Super::BeginPlay();

	if (AIBehaviour != nullptr)
		RunBehaviorTree(AIBehaviour);

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), PlayerPawn->GetActorLocation());
	//GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
}

bool AShooterAIController::IsDead() const
{
	AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn());

	if (ControlledCharacter != nullptr)
		return ControlledCharacter->IsDead();


	return true;
}

//void AShooterAIController::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//
//	//MoveToActor(PlayerPawn, 200);
//
//	//APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
//	//if (LineOfSightTo(PlayerPawn))
//	//{
//	//	//SetPlayerLocation
//	//	GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), PlayerPawn->GetActorLocation());
//	//	
//	//	//SetLastKnownPlayerLocation
//	//	GetBlackboardComponent()->SetValueAsVector(TEXT("LastKnownPlayerLocation"), PlayerPawn->GetActorLocation());
//	//}
//	//else
//	//{
//	//	GetBlackboardComponent()->ClearValue(TEXT("PlayerLocation"));
//	//}
//}

//Chase Custom molto blando
//APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
//if (LineOfSightTo(PlayerPawn))
//{
//	//la priorit� del MoveToActor � inferiore a quella del SetFocus. 
//	SetFocus(PlayerPawn, EAIFocusPriority::Gameplay);
//	MoveToActor(PlayerPawn, AcceptanceRadius);
//}
//else
//{
//	//Sul Clear va reimpostata la categoria usata prima perch� � quella che deve essere pulita.
//	ClearFocus(EAIFocusPriority::Gameplay);
//	StopMovement();
//}