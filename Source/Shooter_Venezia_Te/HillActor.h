// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HillActor.generated.h"

UCLASS()
class SHOOTER_VENEZIA_TE_API AHillActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AHillActor();

	UPROPERTY(EditAnywhere)
		class USphereComponent* Sphere;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndexType, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	bool PlayerWin = false;
	FTimerHandle BeginTimer;
	FTimerHandle EndTimer;

	class AShooterCharacter* Gino;
	class AKingOfTheHillGameModeBase* KOTH;

	void IncreasePoints();

	void DecreasePoints();

	void AddPoints(AShooterCharacter* OtherActor);
	void RemovePoints(AShooterCharacter* OtherActor);

protected:
	virtual void BeginPlay() override;	
};