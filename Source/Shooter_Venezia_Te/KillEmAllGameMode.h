// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Shooter_Venezia_TeGameModeBase.h"
#include "ShooterPlayerController.h"
#include "KillEmAllGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_VENEZIA_TE_API AKillEmAllGameMode : public AShooter_Venezia_TeGameModeBase
{
	GENERATED_BODY()

private:
	virtual void BeginPlay() override;

public:
	virtual void PawnKilled(APawn* PawnKilled) override;

private:
	void EndGame(bool bIsPlayerWinner);

	UPROPERTY(EditAnywhere)
		TArray<FVector> SpawnerVector;

	UPROPERTY(EditAnywhere)
		TArray<FRotator> SpawnerRotator;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AEnemySpawnerActor> SpawnerActor;

	//UPROPERTY(EditAnywhere)
	//	TArray<TSubclassOf<class AEnemySpawnerActor>> SpawnerVector;
};