// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "Shooter_Venezia_TeGameModeBase.h"
#include "HillActor.h"


#pragma region Contructor
// Sets default values
AShooterCharacter::AShooterCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}
#pragma endregion

#pragma region BeginPlay
// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	for (const TSubclassOf<AGun>& Default : GunClass)
	{
		if (!Default) continue;

		//spawn l'actor che ci serve
		Gun = GetWorld()->SpawnActor<AGun>(Default);
		//nascondiamo l'actor esistente
		GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
		//portiamo l'actor spawnato dove serve
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("weaponSocket"));
		//questo � il riferimento al nostro character
		Gun->SetOwner(this);

		const int32 Index = Weapons.Add(Gun);
		if (Index == CurrentIndex)
		{
			CurrentWeapon = Gun;
			OnRep_CurrentWeapon(nullptr);
		}
	}
}
#pragma endregion

#pragma region ChangeWeapon
void AShooterCharacter::OnRep_CurrentWeapon(const AGun* OldWeapon)
{
	if (CurrentWeapon)
	{
		//if (!CurrentWeapon->CurrentOwner)
		//{
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("weaponSocket"));
		CurrentWeapon->CurrentOwner = this;
		//}
		CurrentWeapon->Mesh->SetVisibility(true);
	}

	if (OldWeapon)
	{
		OldWeapon->Mesh->SetVisibility(false);
	}
}

void AShooterCharacter::EquipWeapon(const int32 Index)
{
	//if (!Weapons.IsValidIndex(Index) || CurrentWeapon == Weapons[Index]) return;

	CurrentIndex = Index;
	const AGun* OldWeapon = CurrentWeapon;
	CurrentWeapon = Weapons[Index];
	OnRep_CurrentWeapon(OldWeapon);
}

#pragma endregion

#pragma region Tick
// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
#pragma endregion

#pragma region Input
#pragma region SetupPlayerInputComponent

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);

	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);

	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Shoot);
	PlayerInputComponent->BindAction(TEXT("Refill"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Refill);
	PlayerInputComponent->BindAction(TEXT("NextWeapon"), EInputEvent::IE_Pressed, this, &AShooterCharacter::NextWeapon);
	PlayerInputComponent->BindAction(TEXT("LastWeapon"), EInputEvent::IE_Pressed, this, &AShooterCharacter::LastWeapon);
	PlayerInputComponent->BindAction(TEXT("Pause"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Pause);
}
#pragma endregion

#pragma region MovementInput
void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}
#pragma endregion

#pragma region MouseCameraInput
void AShooterCharacter::LookUpRate(float AxisValue)
{
	//questo serve per slegarci dal Frame Rate
	AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}
#pragma endregion

#pragma region GunInput
#pragma region Shoot

void AShooterCharacter::Shoot()
{
	if (CurrentIndex == 0)
	{
		Gun->isRiffle = true;
		Gun->PullRiffleTrigger();
	}
	else
	{
		Gun->isRiffle = false;
		Gun->PullGrenadeLauncherTrigger();
	}

}
#pragma endregion

#pragma region Refill
void AShooterCharacter::Refill()
{
	if (Gun->RiffleAmmo < Gun->RiffleAmmoAmount)
	{
		Gun->RiffleAmmo += Gun->RiffleAmmoShoot;
		Gun->RiffleAmmoAmount -= Gun->RiffleAmmoShoot;
		Gun->RiffleAmmoShoot = 0;
	}
	else
	{
		Gun->RiffleAmmoAmount = 0;
	}

	if (Gun->GrenadeLauncherAmmo < Gun->GrenadeLauncherAmmoAmount)
	{
		Gun->GrenadeLauncherAmmo += Gun->GrenadeLauncherAmmoShoot;
		Gun->GrenadeLauncherAmmoAmount -= Gun->GrenadeLauncherAmmoShoot;
		Gun->GrenadeLauncherAmmoShoot = 0;
	}
	else
	{
		Gun->GrenadeLauncherAmmoAmount = 0;
	}
}
void AShooterCharacter::HealthRegen()
{
	Health += HealthAmount;
}
#pragma endregion

#pragma region Change Weapon
void AShooterCharacter::NextWeapon()
{
	const int32 Index = Weapons.IsValidIndex(CurrentIndex + 1) ? CurrentIndex + 1 : 0;

	EquipWeapon(Index);
}

void AShooterCharacter::LastWeapon()
{
	const int32 Index = Weapons.IsValidIndex(CurrentIndex - 1) ? CurrentIndex - 1 : Weapons.Num() - 1;

	EquipWeapon(Index);
}
void AShooterCharacter::Pause()
{
	if (!bInPause)
	{
		bInPause = true;

		UE_LOG(LogTemp, Warning, TEXT("Pause"));
	}
	else
	{
		bInPause = false;
		UE_LOG(LogTemp, Warning, TEXT("ExitPause"));
	}
}

#pragma endregion
#pragma endregion
#pragma endregion

#pragma region TakeDamage

float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	//Questo fa si che prende il valore minimo tra i due facendo si che finch� la vita non � minore di Health allora prende come Valore DamageToApply
	DamageToApply = FMath::Min(Health, DamageToApply);

	Health -= DamageToApply;

	if (IsDead())
	{
		AShooter_Venezia_TeGameModeBase* GameMode = GetWorld()->GetAuthGameMode<AShooter_Venezia_TeGameModeBase>();
		if (GameMode != nullptr)
		{
			GameMode->PawnKilled(this);
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DamageToApply;
}
#pragma endregion

#pragma region Function for Blueprint
#pragma region Life
bool AShooterCharacter::IsDead() const
{
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}
#pragma endregion

#pragma region Riffle

int AShooterCharacter::GetRiffleAmmo() const
{
	return  Gun->RiffleAmmo;
}

int AShooterCharacter::GetRiffleAmmoAmount() const
{
	return Gun->RiffleAmmoAmount;
}
#pragma endregion

#pragma region GrenadeLauncher

int AShooterCharacter::GetGrenadeLauncherAmmo() const
{
	return Gun->GrenadeLauncherAmmo;
}

int AShooterCharacter::GetGrenadeLauncherAmmoAmount() const
{
	return Gun->GrenadeLauncherAmmoAmount;
}
#pragma endregion

float AShooterCharacter::PointsABC() const
{
	return Points;
}
#pragma endregion

float AShooterCharacter::GetPoint()
{
	AddPoint(Points);

	return Points;
}

void AShooterCharacter::AddPoint(float Value)
{
	Points += Value;
}