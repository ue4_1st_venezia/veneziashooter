// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectGameModeInstance.h"
#include "Kismet/GameplayStatics.h"

void USelectGameModeInstance::PlayGame(FString Option)
{
	UGameplayStatics::OpenLevel(this, MapName, false, Option);
}