// Fill out your copyright notice in the Description page of Project Settings.


#include "HillActor.h"
#include "Components/SphereComponent.h"
#include "ShooterCharacter.h"
#include "KingOfTheHillGameModeBase.h"
#include "ShooterPlayerController.h"
#include "KingOfTheHillAIController.h"
#include "EngineUtils.h"
#include "KingOfTheHillAIController.h"

// Sets default values
AHillActor::AHillActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	Sphere->SetSphereRadius(200.f);
	Sphere->SetGenerateOverlapEvents(true);
	//Sphere->SetupAttachment(Sphere); //???Ok Unreal
}

void AHillActor::BeginPlay()
{
	Super::BeginPlay();
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &AHillActor::OnOverlapBegin);
	Sphere->OnComponentEndOverlap.AddDynamic(this, &AHillActor::OnOverlapEnd);
}


void AHillActor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndexType, bool bFromSweep, const FHitResult& SweepResult)
{
	Gino = Cast <AShooterCharacter>(OtherActor);
	if (OtherActor != nullptr)
	{
		GetWorldTimerManager().ClearTimer(EndTimer);
		GetWorldTimerManager().SetTimer(BeginTimer, this, &AHillActor::IncreasePoints, .5f, true);
	}
}

void AHillActor::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GetWorldTimerManager().ClearTimer(BeginTimer);
	GetWorldTimerManager().SetTimer(EndTimer, this, &AHillActor::DecreasePoints, .5f, true);

	if (Gino->Points < 0)
	{
		Gino->Points = 0;
	}
}

void AHillActor::IncreasePoints()
{
	if (Gino)
	{
		AddPoints(Gino);
	}
}

void AHillActor::DecreasePoints()
{
	if (Gino)
	{
		RemovePoints(Gino);
	}
}

void AHillActor::AddPoints(AShooterCharacter* OtherActor)
{
	OtherActor->AddPoint(1);

	if (Gino->Points >= Gino->MaxPoint)
	{
		Gino->Points = Gino->MaxPoint;
	}
}

void AHillActor::RemovePoints(AShooterCharacter* OtherActor)
{
	OtherActor->AddPoint(-1);

	if (Gino->Points < 0)
	{
		Gino->Points = 0;
	}
}