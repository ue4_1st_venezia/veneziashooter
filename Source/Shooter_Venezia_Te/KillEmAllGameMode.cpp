// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ShooterAIController.h"
#include "EnemySpawnerActor.h"

void AKillEmAllGameMode::BeginPlay()
{	
	for (int i = 0; i < SpawnerVector.Num(); i++)
	{
		GetWorld()->SpawnActor<AEnemySpawnerActor>(SpawnerActor, SpawnerVector[i], SpawnerRotator[i]);
	}
}

void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());

	if (PlayerController != nullptr)
	{
		AKillEmAllGameMode::EndGame(false);
	}

	for (AShooterAIController* ShooterController : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (ShooterController->IsDead() == false)
		{
			return;
		}
	}

	AKillEmAllGameMode::EndGame(true);
}

void AKillEmAllGameMode::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		//Metodo con if
		/*bool bIsPlayerController = Controller->IsPlayerController();

		if (bIsPlayerWinner)
		{
			Controller->GameHasEnded(nullptr, bIsPlayerController);
		}
		else
		{
			Controller->GameHasEnded(nullptr, !bIsPlayerController);
		}*/

		//Metodo con una riga
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}
